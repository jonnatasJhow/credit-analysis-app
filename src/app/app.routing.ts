import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './client/client.component';
import { CreditAnalysisComponent } from './credit-analysis/credit-analysis.component';

const APP_ROUTES: Routes = [
    { path: 'clients/:id/credit-analysis', component: CreditAnalysisComponent },
    { path: 'clients', component: ClientComponent },
    { path: '', redirectTo: 'clients', pathMatch: 'full' }
];

export const routing:  ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

