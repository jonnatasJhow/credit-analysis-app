import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';
import { Risk } from '../model/Risk';

@Injectable({
  providedIn: 'root'
})
export class RiskService {

  constructor(private http: Http) { }

  getRisks() {
    return this.http.get(environment.baseUrl + `/credit-analysis/api/risks`)
    .toPromise()
    .then(data => <Risk[]>data.json());
  }
}
