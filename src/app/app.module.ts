import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { ClientComponent } from './client/client.component';
import { ClientService } from './client/client.service';
import { CreditAnalysisComponent } from './credit-analysis/credit-analysis.component';
import { CreditAnalysisService } from './credit-analysis/credit-analysis.service';

@NgModule({
  declarations: [
    AppComponent,
    CreditAnalysisComponent,
    ClientComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
  ],
  providers: [ CreditAnalysisService, ClientService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
