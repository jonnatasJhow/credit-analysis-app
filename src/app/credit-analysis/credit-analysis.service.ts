import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment';
import { CreditAnalysis } from '../model/CreditAnalysis';

@Injectable({
  providedIn: 'root'
})
export class CreditAnalysisService {

  constructor(private http: Http) {
  }

  save(creditAnalysis: CreditAnalysis) {
    return this.http.post(environment.baseUrl + `/credit-analysis/api/credit-analysis`, creditAnalysis).toPromise();
  }
}
