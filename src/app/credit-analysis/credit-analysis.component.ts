import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '../client/client.service';
import { Client } from '../model/Client';
import { CreditAnalysis } from '../model/CreditAnalysis';
import { Risk } from '../model/Risk';
import { RiskService } from '../risk/risk.service';
import { CreditAnalysisService } from './credit-analysis.service';

@Component({
  selector: 'app-credit-analysis',
  templateUrl: './credit-analysis.component.html',
  styleUrls: ['./credit-analysis.component.css']
})
export class CreditAnalysisComponent implements OnInit {

  creditAnalysis: CreditAnalysis = new CreditAnalysis();
  risks: Risk[] = [];
  client: Client = new Client();

  constructor(private route: ActivatedRoute,
    private creditAnalysisService: CreditAnalysisService, private clientService: ClientService, private riskService: RiskService) {
  }

  ngOnInit() {
    this.loadRisks();
      this.clientService.getClient(this.route.snapshot.params['id'])
      .then(data => this.client = data);
  }

  loadRisks() {
    this.riskService.getRisks()
    .then(data => {
      this.risks = data;
      this.creditAnalysis['risk'] = this.risks[0];
    });
  }

  onSubmit(form) {
    this.creditAnalysis = form.value;
    this.creditAnalysis.client = this.client;

    this.creditAnalysisService.save(this.creditAnalysis)
    .then(data => this.updateClient());
  }

  updateClient() {
    this.clientService.getClient(this.client.id)
    .then(data => this.client = data);
  }
}
