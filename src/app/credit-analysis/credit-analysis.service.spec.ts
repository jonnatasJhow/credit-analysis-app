import { TestBed, inject } from '@angular/core/testing';

import { CreditAnalysisService } from './credit-analysis.service';

describe('CreditAnalysisService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreditAnalysisService]
    });
  });

  it('should be created', inject([CreditAnalysisService], (service: CreditAnalysisService) => {
    expect(service).toBeTruthy();
  }));
});
