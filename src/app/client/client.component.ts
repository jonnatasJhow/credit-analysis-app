import { Component, OnInit } from '@angular/core';
import { Client } from '../model/Client';
import { ClientService } from './client.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clients: Client[] = [];
  analyze = false;

  selectedClient: Client;

  constructor(private clientService: ClientService) {
    this.clientService.getClients()
    .then(data => this.clients = data);
  }

  ngOnInit() {
  }

  analyzer(client: Client) {
    this.analyze = !this.analyze;

    if (this.analyze) {
      this.selectedClient = client;
    } else {
      this.selectedClient = null;
    }
  }

}
