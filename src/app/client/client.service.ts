import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from 'src/environments/environment.prod';
import { Client } from '../model/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: Http) { }

  getClients() {
    return this.http.get(environment.baseUrl + `/credit-analysis/api/clients`)
    .toPromise()
    .then(data => <Client[]>data.json());
  }

  getClient(clientId: number) {
    return this.http
    .get(environment.baseUrl + `/credit-analysis/api/clients/` + clientId)
    .toPromise()
    .then(data => <Client>data.json());
  }
}
