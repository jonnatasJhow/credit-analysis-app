import { CreditAnalysis } from "./CreditAnalysis";

export class Client {

    constructor(public id?: number, public name?: string, public creditAnalysis?: CreditAnalysis[]) {
    }
}
