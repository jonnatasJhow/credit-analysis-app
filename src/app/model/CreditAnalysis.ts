import { Client } from './Client';
import { Risk } from './Risk';

export class CreditAnalysis {

    constructor(private id?: number, public client?: Client, private creditLimit?: number, public risk?: Risk,
        private creationDate?: string) {

        this.risk = this.risk  ? this.risk : new Risk();
    }
}
